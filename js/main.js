(function () {

Vue.component('ghetto-sound', {
    template: `<div class="btn audio-btn" v-on:click="click">
        <label>{{ asset.name }}</label>
        <span v-bind:class="[\'status-icon\', { playing: playing }]"></span>
    </div>`,
    props: {
        asset: Object,
    },
    data: function () {
        return {
            audio: null,
            playing: false,
        };
    },
    methods: {
        click: function (event) {
            if (this.playing) {
                this.stop();
            }
            else {
                this.play();
            }
        },
        play: function () {
            // Make sure the actual audio element exists.
            if (!this.audio) {
                this.audio = new Audio('audio/' + this.asset.resource);
            }

            // Emit an event so that other playing sounds can be stopped.
            this.$emit('ghetto-playsound');

            this.audio.load();
            this.audio.play();
            this.audio.onended = function () {
                this.playing = false;
            }.bind(this);
            this.playing = true;
        },
        stop: function () {
            if (this.audio) {
                this.audio.pause();
                this.playing = false;
            }
        },
    },
});

var app = new Vue({
    el: '#soundboard',
    data: {
        title: '',
        ready: false,
        assets: [],
    },
    methods: {
        stopAllPlayingSounds: function () {
            // Find all sounds currently playing and stop them.
            this.$refs.sound.forEach(function (item) {
                if (item.playing) {
                    item.stop();
                }
            });
        },
    },
});

// This is a the request to get the main JSON file that drives the website.
// Only load the view when that file is downloaded.
GHETTO_SOURCE_REQUEST.then(function (source) {
    app.$data.ready = true;
    app.$data.title = source.title;
    app.$data.assets = source.assets;
});

})();
